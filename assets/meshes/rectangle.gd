extends StaticBody

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	pass


func _on_Ball_body_enter( body ):
	hide()
	get_node(".").set_collision_mask(0)
	get_node(".").set_layer_mask(0)
