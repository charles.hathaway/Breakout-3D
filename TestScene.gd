extends Spatial

export (int) var Rows
export (int) var Columns
export (PackedScene) var BlockType
const PADDLE_SPEED = 1
var paddle_size
var pending_restart = false

var cameraLeftBound
var cameraRightBound

func _ready():
	set_process(true)
	get_node("Player").set_boundaries(get_node("Boundaries/Left"), get_node("Boundaries/Right"))
	get_node("Player").disable_player_input(get_node("Ball"))
	make_cubes()

func _process(delta):
	if pending_restart:
		pending_restart = false
		start()

func start():
	get_node("Player").set_translation(get_node("PlayerSpawn").get_translation())
	get_node("Ball").set_translation(get_node("BallSpawn").get_translation())
	get_node("Player").enable_player_input()
	destroy_cubes()
	make_cubes()
	get_node("GameOverlay").show()
	get_node("Ball").score = 0
	get_node("Ball").show_score()

func destroy_cubes():
	for c in get_node(".").get_children():
		if c.is_in_group("blocks"):
			c.queue_free()

func make_cubes():
	# Contruct some cubes
	var translate_block = get_node("Block Start").get_translation()
	var offset_x = 0.0
	var offset_y = 0.0
	for r in range(Rows):
		for c in range(Columns):
			var block = BlockType.instance()
			add_child(block)
			block.add_to_group("blocks")
			block.set_translation(Vector3(translate_block.x + offset_x, translate_block.y + offset_y, translate_block.z))
			offset_x -= .5
		offset_x = 0
		offset_y -= .15

func _on_Left_body_enter( body ):
	print("Something entered the left body")


func _on_TitleScreen_start_game():
	pending_restart = true
