extends RigidBody

# class member variables go here, for example:
# var a = 2
export (float) var BALL_SPEED
export (float) var MAX_BALL_SPEED
export (NodePath) var game_overlay
var score = 0
var angle_of_hit
var start_velocity

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	pass

func show_score():
	get_node(str(game_overlay, "/Score")).set_text(str(score))

func _on_Ball_body_enter( body ):
	start_velocity = get_node(".").get_linear_velocity()
	print("Hit " + body.get_name())
	if body.get_name() == "Player":
		var my_translate = get_node(".").get_translation()
		var ot_translate = body.get_translation()
		var num = (my_translate.x - ot_translate.x)
		var den = (my_translate.y - ot_translate.y)
		var new_direction = tan(num / den)
		get_node(".").set_linear_velocity(Vector3(clamp(BALL_SPEED * new_direction, -MAX_BALL_SPEED, MAX_BALL_SPEED), BALL_SPEED, 0))
	elif body.is_in_group("blocks"):
		body.hide()
		body.set_collision_mask(0)
		body.queue_free()
		score += 1
		show_score()
