extends Spatial

# class member variables go here, for example:
const PADDLE_SPEED = 0.25
var boundaries
var mybox
var playable = false
var track

func _ready():
	set_process(true)

func set_boundaries(left, right):
	boundaries = [left, right]

func _process(delta):
	if playable:
		if Input.is_action_pressed("game_left") and get_node(".").get_translation().x < 7.3:
			get_node(".").translate(Vector3(PADDLE_SPEED, 0.0, 0.0))
		elif Input.is_action_pressed("game_right") and get_node(".").get_translation().x > -7.3:
			get_node(".").translate(Vector3(-PADDLE_SPEED, 0.0, 0.0))
		if Input.is_action_pressed("debug"):
			print("Position: " + str(get_node(".").get_translation()))
	elif track:
		var trans = track.get_translation()
		if trans.x > get_node(".").get_translation().x and get_node(".").get_translation().x < 7.3:
			get_node(".").translate(Vector3(clamp(trans.x - get_node(".").get_translation().x, 0.0, PADDLE_SPEED), 0.0, 0.0))
		elif trans.x < get_node(".").get_translation().x and get_node(".").get_translation().x > -7.3:
			get_node(".").translate(Vector3(clamp(trans.x - get_node(".").get_translation().x, -PADDLE_SPEED, 0.0), 0.0, 0.0))

func disable_player_input(item_to_track):
	if item_to_track != null:
		track = item_to_track

func enable_player_input():
	playable = true

func _on_Area_area_enter( area ):
	print("Entered area")


func _on_Area_body_enter( body ):
	print("Entered body")
